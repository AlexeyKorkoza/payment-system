import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NzLayoutModule } from 'ng-zorro-antd/layout';

import { AppComponent } from './app.component';
import { PaymentModule } from './features/payment/payment.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NzLayoutModule,
    PaymentModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
