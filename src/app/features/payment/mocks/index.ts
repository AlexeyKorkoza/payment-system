export const firstAddPaymentMock = {
  name: 'football',
  cost: 4,
};

export const secondAddPaymentMock = {
  name: 'gym',
  cost: 10,
};

export const selectFirstMonthBody = {
  paymentIndex: 0,
  monthIndex: 1,
};

export const selectSecondMonthBody = {
  ...selectFirstMonthBody,
  monthIndex: 4,
};
