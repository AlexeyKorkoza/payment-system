import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { PaymentModel } from '../models/payment.model';
import { SelectedMonthsModel } from '../models/selected-months.model';
import { CreatePaymentModel } from '../models/create-payment.model';
import { PaymentSubjectModel } from '../models/payment-subject.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  private paymentSubject = new BehaviorSubject<PaymentSubjectModel>({
    payments: [],
    sumPayments: 0,
  });
  private payments: Array<PaymentModel> = [];
  private sumPayments = 0;

  public paymentSubscription: Observable<PaymentSubjectModel> = this.paymentSubject.asObservable();

  // for unit tests
  get getCurrentPayments(): Array<PaymentModel> {
    return this.payments;
  }

  // for unit tests
  get getSumPayments(): number {
    return this.sumPayments;
  }

  getCountDaysInMonth(monthIndex: number): number {
    const currentDate = new Date();
    const year = currentDate.getFullYear();

    if (/8|3|5|10/.test(monthIndex.toString())) {
      return 30;
    }

    if (monthIndex !== 1) {
      return 31;
    }

    if (year % 4 === 0) {
      return 29;
    }

    return 28;
  }

  private sendUpdatedData(): void {
    this.paymentSubject.next({
      payments: this.payments,
      sumPayments: this.sumPayments,
    });
  }

  private calculateSumPaymentsForYear(): void {
     this.sumPayments = this.payments.reduce((sum: number, payment: PaymentModel) => {
      const { cost, selectedMonths } = payment;

      const paymentYear = Object
        .values(selectedMonths)
        .reduce((yearSum: number, monthIndex: number) => {
          const paymentMonth = this.getCountDaysInMonth(monthIndex) * cost;

          return yearSum + paymentMonth;
        }, 0);

      return sum + paymentYear;
    }, 0);
     this.sendUpdatedData();
  }

  addPayment(payment: CreatePaymentModel): void {
    this.payments.push({
      ...payment,
      selectedMonths: {},
    });
    this.calculateSumPaymentsForYear();
  }

  deletePayment(paymentIndex: number): void {
    this.payments = this.payments.filter((payment: PaymentModel, index: number) => paymentIndex !== index);
    this.calculateSumPaymentsForYear();
  }

  selectMonth(paymentIndex: number, monthIndex: number): void {
    this.payments = this.payments.map((payment: PaymentModel, index: number) => {
      if (paymentIndex !== index) {
        return payment;
      }

      const { name, cost, selectedMonths } = payment;

      return {
        name,
        cost,
        selectedMonths: {
          ...selectedMonths,
          [`${monthIndex}`]: monthIndex,
        }
      };
    });
    this.calculateSumPaymentsForYear();
  }

  unselectMonth(paymentIndex: number, monthIndex: number): void {
    this.payments = this.payments.map((payment: PaymentModel, index: number) => {
      if (paymentIndex !== index) {
        return payment;
      }

      const { name, cost, selectedMonths } = payment;
      const updatedListMonths = Object
        .keys(selectedMonths)
        .reduce((acc: SelectedMonthsModel, item: string) => {
          if (+item === monthIndex) {
            return acc;
          }

          acc[item] = +item;

          return acc;
        }, {});

      return {
        name,
        cost,
        selectedMonths: updatedListMonths,
      };
    });
    this.calculateSumPaymentsForYear();
  }
}
