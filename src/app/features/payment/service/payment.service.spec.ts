import { TestBed } from '@angular/core/testing';

import { PaymentService } from './payment.service';
import { firstAddPaymentMock, secondAddPaymentMock, selectFirstMonthBody, selectSecondMonthBody } from '../mocks';

describe('PaymentService', () => {
  let paymentService: PaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
       PaymentService,
      ]
    });

    paymentService = TestBed.inject(PaymentService);
  });

  it('should be created', () => {
    expect(paymentService).toBeTruthy();
  });

  describe('when calls the addPayment method', () => {
    it('should add new payment to the list', () => {
      paymentService.addPayment(firstAddPaymentMock);

      expect(paymentService.getCurrentPayments.length).toBe(1);
      expect(paymentService.getCurrentPayments[0].name).toBe(firstAddPaymentMock.name);
      expect(paymentService.getCurrentPayments[0].cost).toBe(firstAddPaymentMock.cost);

      expect(paymentService.getSumPayments).toBe(0);
    });
  });

  describe('when calls the deletePayment method', () => {
    it('should delete the payment and the payment list has only one payment', () => {
      paymentService.addPayment(firstAddPaymentMock);
      paymentService.addPayment(secondAddPaymentMock);

      expect(paymentService.getCurrentPayments.length).toBe(2);

      const paymentIndex = 0;
      paymentService.deletePayment(paymentIndex);
      expect(paymentService.getCurrentPayments[0].name).toBe(secondAddPaymentMock.name);
      expect(paymentService.getCurrentPayments[0].cost).toBe(secondAddPaymentMock.cost);
    });
  });

  describe('when calls the selectMonth method', () => {
    it('should select the month', () => {
      paymentService.addPayment(firstAddPaymentMock);

      const monthIndex = 2;
      paymentService.selectMonth(0, monthIndex);

      expect(paymentService.getCurrentPayments[0].name).toBe(firstAddPaymentMock.name);
      expect(paymentService.getCurrentPayments[0].cost).toBe(firstAddPaymentMock.cost);
      expect(paymentService.getCurrentPayments[0].selectedMonths).toEqual({
        2: 2,
      });

      const firstPaymentMonthSum = paymentService.getCountDaysInMonth(monthIndex) * firstAddPaymentMock.cost;
      expect(paymentService.getSumPayments).toBe(firstPaymentMonthSum);
    });

    it('should select the month in the second payment', () => {
      paymentService.addPayment(firstAddPaymentMock);
      paymentService.addPayment(secondAddPaymentMock);

      const monthIndex = 2;
      paymentService.selectMonth(0, monthIndex);

      expect(paymentService.getCurrentPayments[0].name).toBe(firstAddPaymentMock.name);
      expect(paymentService.getCurrentPayments[0].cost).toBe(firstAddPaymentMock.cost);
      expect(paymentService.getCurrentPayments[0].selectedMonths).toEqual({
        2: 2,
      });

      const firstPaymentMonthSum = paymentService.getCountDaysInMonth(monthIndex) * firstAddPaymentMock.cost;
      expect(paymentService.getSumPayments).toBe(firstPaymentMonthSum);
    });
  });

  describe('when calls the unselectMonth method', () => {
    it('should unselect the month', () => {
      paymentService.addPayment(firstAddPaymentMock);

      paymentService.selectMonth(selectFirstMonthBody.paymentIndex, selectFirstMonthBody.monthIndex);
      paymentService.selectMonth(selectSecondMonthBody.paymentIndex, selectSecondMonthBody.monthIndex);

      paymentService.unselectMonth(0, selectFirstMonthBody.monthIndex);

      expect(paymentService.getCurrentPayments[0].name).toBe(firstAddPaymentMock.name);
      expect(paymentService.getCurrentPayments[0].cost).toBe(firstAddPaymentMock.cost);
      expect(paymentService.getCurrentPayments[0].selectedMonths).toEqual({
        4: 4,
      });

      const firstPaymentMonthSum = paymentService.getCountDaysInMonth(selectSecondMonthBody.monthIndex) * firstAddPaymentMock.cost;
      expect(paymentService.getSumPayments).toBe(firstPaymentMonthSum);
    });

    it('should unselect the month in the second payment', () => {
      paymentService.addPayment(firstAddPaymentMock);
      paymentService.addPayment(secondAddPaymentMock);

      paymentService.selectMonth(0, selectFirstMonthBody.monthIndex);

      const monthIndex = 3;
      paymentService.selectMonth(0, monthIndex);

      paymentService.unselectMonth(0, selectFirstMonthBody.monthIndex);

      expect(paymentService.getCurrentPayments[0].name).toBe(firstAddPaymentMock.name);
      expect(paymentService.getCurrentPayments[0].cost).toBe(firstAddPaymentMock.cost);
      expect(paymentService.getCurrentPayments[0].selectedMonths).toEqual({
        3: monthIndex,
      });

      const firstPaymentMonthSum = paymentService.getCountDaysInMonth(monthIndex) * firstAddPaymentMock.cost;
      expect(paymentService.getSumPayments).toBe(firstPaymentMonthSum);
    });
  });

  describe('when calls the getCountDaysInMonth method', () => {
    beforeEach(() => {
      jasmine.clock().install();
    });

    afterEach(() => {
      jasmine.clock().uninstall();
    });

    it('should return 31 days in the month', () => {
      expect(paymentService.getCountDaysInMonth(2)).toBe(31);
      expect(paymentService.getCountDaysInMonth(6)).toBe(31);
      expect(paymentService.getCountDaysInMonth(11)).toBe(31);
    });

    it('should return 30 days in the month', () => {
      expect(paymentService.getCountDaysInMonth(3)).toBe(30);
      expect(paymentService.getCountDaysInMonth(5)).toBe(30);
      expect(paymentService.getCountDaysInMonth(10)).toBe(30);
    });

    it('should return 29 days in the February', () => {
      let leapYearMonth = new Date(2020, 1);
      jasmine.clock().mockDate(leapYearMonth);
      expect(paymentService.getCountDaysInMonth(1)).toBe(29);

      leapYearMonth = new Date(2000, 1);
      jasmine.clock().mockDate(leapYearMonth);
      expect(paymentService.getCountDaysInMonth(1)).toBe(29);
    });

    it('should return 28 days in the February', () => {
      let noLeapYearMonth = new Date(2019, 1);
      jasmine.clock().mockDate(noLeapYearMonth);
      expect(paymentService.getCountDaysInMonth(1)).toBe(28);

      noLeapYearMonth = new Date(2018, 1);
      jasmine.clock().mockDate(noLeapYearMonth);
      expect(paymentService.getCountDaysInMonth(1)).toBe(28);
    });
  });
});
