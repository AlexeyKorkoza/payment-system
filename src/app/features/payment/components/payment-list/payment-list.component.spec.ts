import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NzTableModule } from 'ng-zorro-antd/table';

import { PaymentListComponent } from './payment-list.component';
import { selectFirstMonthBody } from '../../mocks';
import { SelectMonthModel } from '../../models/select-month.model';

describe('TableComponent', () => {
  let component: PaymentListComponent;
  let fixture: ComponentFixture<PaymentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentListComponent],
      imports: [NzTableModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentListComponent);
    component = fixture.componentInstance;
    component.payments = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should raise the deletePayment event when clicked', () => {
    const paymentIndex = 0;
    component.deletePayment.subscribe((data: number) => {
      expect(data).toBe(paymentIndex);
    });
    component.handleDeleteClick(paymentIndex);
  });

  it('should raise the selectMonth event when clicked', () => {
    component.selectMonth.subscribe((data: SelectMonthModel) => {
      expect(data.paymentIndex).toBe(selectFirstMonthBody.paymentIndex);
      expect(data.monthIndex).toBe(selectFirstMonthBody.monthIndex);
    });
    component.handleSelectedMonth(selectFirstMonthBody.paymentIndex, selectFirstMonthBody.monthIndex);
  });

  it('should raise the unselectMonth event when clicked', () => {
    component.unselectMonth.subscribe((data: SelectMonthModel) => {
      expect(data.paymentIndex).toBe(selectFirstMonthBody.paymentIndex);
      expect(data.monthIndex).toBe(selectFirstMonthBody.monthIndex);
    });
    component.handleUnselectedMonth(selectFirstMonthBody.paymentIndex, selectFirstMonthBody.monthIndex);
  });
});
