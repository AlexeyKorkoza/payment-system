import { Component, Input, EventEmitter, Output } from '@angular/core';

import { MONTHS } from '../../constants';
import { PaymentModel } from '../../models/payment.model';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.css']
})
export class PaymentListComponent {
  months = MONTHS;
  noResultText = 'У вас нет каких-либо добавленных платежей';

  @Input() payments: Array<PaymentModel>;

  @Output() deletePayment = new EventEmitter<number>();
  @Output() selectMonth = new EventEmitter();
  @Output() unselectMonth = new EventEmitter();

  handleDeleteClick(paymentIndex: number): void {
    this.deletePayment.emit(paymentIndex);
  }

  handleSelectedMonth(paymentIndex: number, monthIndex: number): void {
    this.selectMonth.emit({
      paymentIndex,
      monthIndex,
    });
  }

  handleUnselectedMonth(paymentIndex: number, monthIndex: number): void {
    this.unselectMonth.emit({
      paymentIndex,
      monthIndex,
    });
  }
}
