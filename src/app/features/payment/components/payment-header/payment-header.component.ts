import { Component, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, AbstractControl } from '@angular/forms';

import { CreatePaymentModel } from '../../models/create-payment.model';

@Component({
  selector: 'app-payment-header',
  templateUrl: './payment-header.component.html',
  styleUrls: ['./payment-header.component.css']
})
export class PaymentHeaderComponent {
  @Output() addPayment = new EventEmitter<CreatePaymentModel>();

  headerPaymentForm = this.fb.group({
    name: ['', Validators.required],
    cost: ['', this.validateCostInput],
  });

  constructor(private fb: FormBuilder) { }

  submitForm(): void {
    this.addPayment.emit(this.headerPaymentForm.value);
    this.headerPaymentForm.patchValue({
      name: '',
      cost: '',
    });
  }

  validateCostInput(control: AbstractControl): {[key: string]: any} | null {
    const cost = control.value;
    const isValidValue = /^[0-9]\d*$/gm.test(cost);
    if (!isValidValue) {
      return { costInputInvalid: true };
    }

    return +cost > 0 ? null : { costInputInvalid: true };
  }
}
