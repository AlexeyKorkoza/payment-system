import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { PaymentHeaderComponent } from './payment-header.component';
import { PaymentModel } from '../../models/payment.model';
import { firstAddPaymentMock } from '../../mocks';

describe('HeaderComponent', () => {
  let component: PaymentHeaderComponent;
  let fixture: ComponentFixture<PaymentHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        FormBuilder,
      ],
    });

    fixture = TestBed.createComponent(PaymentHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when calls the submitForm method', () => {
    it('should have filled in both fields after clicking on the button', () => {
      component.headerPaymentForm.controls.name.setValue(firstAddPaymentMock.name);
      component.headerPaymentForm.controls.cost.setValue(firstAddPaymentMock.cost);

      component.addPayment.subscribe((payment: PaymentModel) => {
        expect(payment.name).toBe(firstAddPaymentMock.name);
        expect(payment.cost).toBe(firstAddPaymentMock.cost);
      });
      component.submitForm();
    });

    it('should have filled in both fields and reset them after clicking on the button', () => {
      component.headerPaymentForm.controls.name.setValue(firstAddPaymentMock.name);
      component.headerPaymentForm.controls.cost.setValue(firstAddPaymentMock.cost);

      component.addPayment.subscribe((payment: PaymentModel) => {
        expect(payment.name).toBe(firstAddPaymentMock.name);
        expect(payment.cost).toBe(firstAddPaymentMock.cost);
      });
      component.submitForm();

      expect(component.headerPaymentForm.controls.name.value).toBe('');
      expect(component.headerPaymentForm.controls.cost.value).toBe('');
    });
  });

  describe('when calls the validateCostInput method', () => {
    beforeEach(() => {
      component.headerPaymentForm.reset();
    });

    it('should return null when value is greater than 0', () => {
      component.headerPaymentForm.controls.cost.setValue('5');
      const result = component.validateCostInput(component.headerPaymentForm.controls.cost);
      expect(result).toBeNull();
    });

    it('should return the error when value is string', () => {
      component.headerPaymentForm.controls.cost.setValue('test');
      const result = component.validateCostInput(component.headerPaymentForm.controls.cost);
      expect(result).toEqual({
       costInputInvalid: true,
      });
    });

    it('should return the error when value is less than 1', () => {
      component.headerPaymentForm.controls.cost.setValue('0');
      const result = component.validateCostInput(component.headerPaymentForm.controls.cost);
      expect(result).toEqual({
        costInputInvalid: true,
      });
    });
  });
});
