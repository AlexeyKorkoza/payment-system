import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentContainerComponent } from './payment-container.component';
import { PaymentHeaderComponent } from '../components/payment-header/payment-header.component';
import { PaymentListComponent } from '../components/payment-list/payment-list.component';
import { PaymentService } from '../service/payment.service';
import {
  firstAddPaymentMock,
  secondAddPaymentMock,
  selectFirstMonthBody,
  selectSecondMonthBody,
} from '../mocks';
import { PaymentModule } from '../payment.module';

describe('PaymentComponent', () => {
  let component: PaymentContainerComponent;
  let fixture: ComponentFixture<PaymentContainerComponent>;
  let paymentService: jasmine.SpyObj<PaymentService>;

  beforeEach(async(() => {
    const paymentServiceSpy = jasmine.createSpyObj('PaymentService',
      ['addPayment', 'deletePayment', 'selectMonth', 'unselectMonth'],
    );

    TestBed.configureTestingModule({
      imports: [
        PaymentModule,
      ],
      declarations: [
        PaymentContainerComponent,
      ],
      providers: [
        PaymentHeaderComponent,
        PaymentListComponent,
        { provide: PaymentService, use: paymentServiceSpy },
      ],
    })
      .compileComponents();

    paymentService = TestBed.inject(PaymentService) as jasmine.SpyObj<PaymentService>;
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentContainerComponent);
    component = fixture.componentInstance;
    component.paymentsSubscription = null;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should have default values before calling ngOnInit method', () => {
    expect(component.sumPayments).toBe(0);
    expect(component.payments.length).toBe(0);
  });

  describe('when calls the addPayment method', () => {
    it('should add one payment to the list', () => {
      component.addPayment(firstAddPaymentMock);
      paymentService.paymentSubscription.subscribe(data => {
        expect(data.payments.length).toBe(1);

        expect(data.payments[0].name).toBe(firstAddPaymentMock.name);
        expect(data.payments[0].cost).toBe(firstAddPaymentMock.cost);
        expect(data.payments[0].selectedMonths).toEqual({});

        expect(data.sumPayments).toBe(0);
      });
    });

    it('should add two payments to the list', () => {
      component.addPayment(firstAddPaymentMock);
      component.addPayment(secondAddPaymentMock);

      paymentService.paymentSubscription.subscribe(data => {
        expect(data.payments.length).toBe(2);

        expect(data.payments[0].name).toBe(firstAddPaymentMock.name);
        expect(data.payments[1].cost).toBe(secondAddPaymentMock.cost);
        expect(data.payments[0].selectedMonths).toEqual({});

        expect(data.sumPayments).toBe(0);
      });
    });
  });

  describe('when calls the selectMonth method', () => {
    it('should select only one month of the payment', () => {
      component.addPayment(firstAddPaymentMock);

      const selectMonthBody = {
        paymentIndex: 0,
        monthIndex: 1,
      };
      component.selectMonth(selectMonthBody);
      paymentService.paymentSubscription.subscribe(data => {
        expect(data.payments.length).toBe(1);

        expect(data.payments[0].name).toBe(firstAddPaymentMock.name);
        expect(data.payments[0].cost).toBe(firstAddPaymentMock.cost);
        expect(data.payments[0].selectedMonths).toEqual({
          1: 1,
        });
        const sumPayments = paymentService.getCountDaysInMonth(selectMonthBody.monthIndex) * firstAddPaymentMock.cost;

        expect(data.sumPayments).toBe(sumPayments);
      });
    });

    it('should select only two months of the payment', () => {
      component.addPayment(firstAddPaymentMock);

      component.selectMonth(selectFirstMonthBody);
      component.selectMonth(selectSecondMonthBody);

      paymentService.paymentSubscription.subscribe(data => {
        expect(data.payments.length).toBe(1);

        expect(data.payments[0].name).toBe(firstAddPaymentMock.name);
        expect(data.payments[0].cost).toBe(firstAddPaymentMock.cost);
        expect(data.payments[0].selectedMonths).toEqual({
          1: 1,
          4: 4,
        });
        const firstMonthSum = paymentService.getCountDaysInMonth(selectFirstMonthBody.monthIndex) * firstAddPaymentMock.cost;
        const secondMonthSum = paymentService.getCountDaysInMonth(selectSecondMonthBody.monthIndex) * firstAddPaymentMock.cost;

        expect(data.sumPayments).toBe(firstMonthSum + secondMonthSum);
      });
    });
  });

  describe('when calls the unselectMonth method', () => {
    it('should unselect only one month of the payment', () => {
      const mock = {
        name: 'football',
        cost: 4,
      };
      component.addPayment(mock);

      component.selectMonth(selectFirstMonthBody);
      component.selectMonth(selectSecondMonthBody);

      component.unselectMonth(selectFirstMonthBody);

      paymentService.paymentSubscription.subscribe(data => {
        expect(data.payments.length).toBe(1);

        expect(data.payments[0].name).toBe(mock.name);
        expect(data.payments[0].cost).toBe(mock.cost);
        expect(data.payments[0].selectedMonths).toEqual({
          4: 4,
        });
        const secondMonthSum = paymentService.getCountDaysInMonth(selectSecondMonthBody.monthIndex) * mock.cost;

        expect(data.sumPayments).toBe(secondMonthSum);
      });
    });
  });

  describe('when calls the deletePayment method', () => {
    it('should delete only one payment of two payments', () => {
      component.addPayment(firstAddPaymentMock);
      component.addPayment(secondAddPaymentMock);

      component.deletePayment(0);

      paymentService.paymentSubscription.subscribe(data => {
        expect(data.payments.length).toBe(1);

        expect(data.payments[0].name).toBe(secondAddPaymentMock.name);
        expect(data.payments[0].cost).toBe(secondAddPaymentMock.cost);
        expect(data.payments[0].selectedMonths).toEqual({});

        expect(data.sumPayments).toBe(0);
      });
    });
  });
});
