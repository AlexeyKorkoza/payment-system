import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { PaymentModel } from '../models/payment.model';
import { PaymentService } from '../service/payment.service';
import { CreatePaymentModel } from '../models/create-payment.model';
import { PaymentSubjectModel } from '../models/payment-subject.model';
import { SelectMonthModel } from '../models/select-month.model';

@Component({
  selector: 'app-payment',
  templateUrl: './payment-container.component.html',
  styleUrls: ['./payment-container.component.css']
})
export class PaymentContainerComponent implements OnDestroy, OnInit {
  payments: Array<PaymentModel> = [];
  paymentsSubscription: Subscription;
  sumPayments = 0;

  constructor(private paymentService: PaymentService) {}

  ngOnInit(): void {
    this.paymentsSubscription = this.paymentService.paymentSubscription
      .subscribe((result: PaymentSubjectModel) => {
        const { payments, sumPayments } = result;
        this.payments = payments;
        this.sumPayments = sumPayments;
      });
  }

  ngOnDestroy(): void {
    if (this.paymentsSubscription) {
      this.paymentsSubscription.unsubscribe();
    }
  }

  deletePayment(paymentIndex: number): void {
    this.paymentService.deletePayment(paymentIndex);
  }

  selectMonth({ paymentIndex, monthIndex }: SelectMonthModel): void {
    this.paymentService.selectMonth(paymentIndex, monthIndex);
  }

  unselectMonth({ paymentIndex, monthIndex }: SelectMonthModel): void {
    this.paymentService.unselectMonth(paymentIndex, monthIndex);
  }

  addPayment(payment: CreatePaymentModel): void {
    this.paymentService.addPayment(payment);
  }
}
