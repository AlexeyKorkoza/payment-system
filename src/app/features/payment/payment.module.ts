import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzTableModule } from 'ng-zorro-antd/table';

import { PaymentContainerComponent } from './container/payment-container.component';
import { PaymentListComponent } from './components/payment-list/payment-list.component';
import { PaymentHeaderComponent } from './components/payment-header/payment-header.component';

@NgModule({
  declarations: [PaymentContainerComponent, PaymentListComponent, PaymentHeaderComponent],
  exports: [
    PaymentContainerComponent
  ],
  imports: [
    CommonModule,
    NzButtonModule,
    NzCheckboxModule,
    NzFormModule,
    NzIconModule,
    NzInputModule,
    NzSpaceModule,
    NzTableModule,
    ReactiveFormsModule,
  ]
})
export class PaymentModule { }
