import { PaymentModel } from './payment.model';

export interface PaymentSubjectModel {
  payments: Array<PaymentModel>;
  sumPayments: number;
}
