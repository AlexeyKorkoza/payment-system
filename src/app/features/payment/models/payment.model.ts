import { SelectedMonthsModel } from './selected-months.model';
import { CreatePaymentModel } from './create-payment.model';

export interface PaymentModel extends CreatePaymentModel {
  selectedMonths: SelectedMonthsModel;
}
