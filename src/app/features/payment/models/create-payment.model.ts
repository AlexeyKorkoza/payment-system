export interface CreatePaymentModel {
  name: string;
  cost: number;
}
