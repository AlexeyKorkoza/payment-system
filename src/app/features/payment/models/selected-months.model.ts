export interface SelectedMonthsModel {
  [key: string]: number;
}
